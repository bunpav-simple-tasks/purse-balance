<?php

namespace App\Services\PurseService\Contract;

use App\Services\PurseService\Dto\PurseDto;

/**
 * Interface BalanceInfoInterface
 *
 * @package App\Services\PurseService\Contract
 */
interface BalanceInfoInterface
{
    /**
     * Возращает баланс кошелька
     *
     * @param int $purseId
     * @return PurseDto
     */
    public function balance(int $purseId) : PurseDto;
}
