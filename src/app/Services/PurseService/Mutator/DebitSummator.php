<?php

namespace App\Services\PurseService\Mutator;

use App\Services\PurseService\Contract\SummatorInterface;

/**
 * Class DebitSummator
 * Выполняет суммирование дебетовой информации
 *
 * @package App\Services\PurseService\Mutator
 */
class DebitSummator implements SummatorInterface
{

    /**
     * @inheritDoc
     */
    public function summ(float $purseBalance, float $operationSumm): float
    {
        return $purseBalance + $operationSumm;
    }
}
