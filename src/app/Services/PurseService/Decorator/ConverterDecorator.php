<?php

namespace App\Services\PurseService\Decorator;

use App\Services\PurseService\PurseModel;
use App\Services\PurseService\Dto\PurseDto;
use App\Services\PurseService\Dto\OperationDto;
use App\Services\PurseService\Mutator\RubToUsdConverter;
use App\Services\PurseService\Mutator\UsdToRubConverter;
use App\Services\PurseService\Exception\UndefinedPurseException;
use App\Services\PurseService\Contract\OperationServiceInterface;

/**
 * Class ConvertorDecorator
 * Декоратор конвертирующий USD <--> RUB
 *
 * @package App\Services\PurseService\Decorator
 */
class ConverterDecorator implements OperationServiceInterface
{
    /** @var OperationServiceInterface  */
    private $service;
    /** @var PurseModel  */
    private $purseModel;
    /** @var RubToUsdConverter  */
    private $rubConvector;
    /** @var UsdToRubConverter  */
    private $usdConvector;

    public function __construct(
        OperationServiceInterface $service,
        PurseModel $purseModel,
        RubToUsdConverter $rubConvector,
        UsdToRubConverter $usdConvector
    ) {
        $this->service = $service;
        $this->rubConvector = $rubConvector;
        $this->usdConvector = $usdConvector;
        $this->purseModel = $purseModel;
    }

    /**
     * @inheritDoc
     */
    public function create(OperationDto $operationDto):PurseDto
    {
        $purse = $this->purseModel->find($operationDto->getPurseId());
        if ($purse === null) {
            throw new UndefinedPurseException('Не известный кошелек!');
        }

        $purseCode = $purse->getCurrency()->getCharCode();

        if ($purseCode !== $operationDto->getCurrencyCode()) {
            $operationDto->setSumm(
                $purseCode === 'USD'
                ? $this->rubConvector->convert($operationDto->getSumm())
                : $this->usdConvector->convert($operationDto->getSumm())
            );
        }

        $operationDto->setCurrencyCode($purseCode);
        return $this->service->create($operationDto);
    }
}
