<?php
return [
    'required' => 'Поле :attribute обязательно для заполнения',
    'numeric'  => 'Поле :attribute должно быть числом',
    'integer'  => 'Поле :attribute должно быть целым числом',
    'in'       => 'Поле :attribute имеет не корректное значение',
];