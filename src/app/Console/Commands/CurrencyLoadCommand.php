<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\CurrencyLoader\Dto\CurrencyDto;
use App\Services\CurrencyLoader\CurrencyLoaderService;

/**
 * Class CurrencyLoadCommand
 * @package App\Console\Commands
 */
class CurrencyLoadCommand extends Command
{
    const SIGNATURE = 'import:currency';

    private const USD = 'R01235';
    private const ENDPOINT = 'http://www.cbr.ru/scripts/XML_daily.asp';

    protected $signature = self::SIGNATURE;

    protected $name = 'Загрузка актуального курса USD';
    protected $description = 'Загружает актуальный курс usd в базу';

    /**
     * Undocumented variable
     *
     * @var \GuzzleHttp\Client
     */
    private $client;
    private $service;
    //
    protected function configure()
    {
        $this
            ->setName(self::SIGNATURE)
            ->setHelp($this->description)
        ;
    }

    /**
     * CurrencyLoadCommand constructor.
     * @param \GuzzleHttp\Client $client
     * @param CurrencyLoaderService $service
     */
    public function __construct(\GuzzleHttp\Client $client, CurrencyLoaderService $service)
    {
        parent::__construct();
        $this->client = $client;
        $this->service = $service;
    }

    /**
     *
     */
    public function handle()
    {
        $responce = $this->client->get(self::ENDPOINT.'?date_req='.date('d/m/Y'));
        $xml = $responce->getBody()->getContents();
        $parse = new \SimpleXMLElement($xml);
        $valute = (array) current($parse->xpath(sprintf("//Valute [@ID='%s']", self::USD)));
        $fmt = numfmt_create('de_DE', \NumberFormatter::DECIMAL);

        $dto = (new CurrencyDto())
                ->setValuteId($valute['@attributes']['ID'])
                ->setNumCode($valute['NumCode'])
                ->setCharCode($valute['CharCode'])
                ->setNominal($valute['Nominal'])
                ->setName($valute['Name'])
                ->setValue(numfmt_parse($fmt, $valute['Value']))
        ;
        
        $result = $this->service->dailyLoad($dto);
        if ($result) {
            $this->info('Курс USD:'. $dto->getValue(). ' успешно загружен');
        } else {
            $this->info('Курс USD:'. $dto->getValue(). ' был загружен ранее');
        }
    }
}
