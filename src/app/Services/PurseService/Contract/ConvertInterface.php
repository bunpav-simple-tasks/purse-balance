<?php

namespace App\Services\PurseService\Contract;

/**
 * Interface ConvertInterface
 * @package App\Services\PurseService\Contract
 */
interface ConvertInterface
{
    /**
     * Выполняет конвертацию переданного значения
     *
     * @param float $value
     * @return float
     */
    public function convert(float $value): float;
}
