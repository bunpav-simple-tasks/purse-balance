<?php


namespace Tests\MockTraits;


use Illuminate\Database\Connection;
use Illuminate\Database\Capsule\Manager;

trait ConnectionMockTrait
{
    private function getConnectionMock(): Connection
    {
        $connection = \Mockery::mock(Connection::class);
        $connection->allows()->beginTransaction()->andReturns(true);
        $connection->allows()->commit()->andReturns(true);
        $connection->allows()->rollback()->andReturns(true);
        $connection->allows([
            'table' => $connection,
            'insert' => $connection,
            'where' => $connection,
            'update' => $connection
        ]);

        return $connection;
    }

    private function getCapsuleMock(): Manager
    {
        $capsule = \Mockery::mock(Manager::class);
        $capsule->allows()->getConnection()->andReturns($this->getConnectionMock());

        return $capsule;
    }
}