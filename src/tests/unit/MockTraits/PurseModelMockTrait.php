<?php


namespace Tests\MockTraits;


use App\Services\CurrencyLoader\Dto\CurrencyDto;
use App\Services\PurseService\Dto\PurseDto;
use App\Services\PurseService\PurseModel;

trait PurseModelMockTrait
{
    private function getPurseModelMock(): PurseModel
    {
        $purseDto = $this->getTestPurseDto();
        $currencyDto = new CurrencyDto();
        $currencyDto->setCharCode($purseDto->getCurrencyCode());

        $purseModel = \Mockery::mock(PurseModel::class);

        $purseModel->allows()->getCurrency()->andReturns($currencyDto);
        $purseModel->allows()->getId()->andReturns($purseDto->getPurseId());
        $purseModel->allows()->getBalance()->andReturns($purseDto->getBalance());
        $purseModel->allows()->find($purseDto->getPurseId())->andReturn($purseModel);

        return $purseModel;
    }

    private function getTestPurseDto(): PurseDto {
        return (new PurseDto())
                    ->setBalance(55)
                    ->setCurrencyCode('RUB')
                    ->setPurseId(123)
        ;
    }
}