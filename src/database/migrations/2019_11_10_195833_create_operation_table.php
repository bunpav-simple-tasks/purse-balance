<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purse_id')->nullable(false);
            $table->enum('cause',['stock','refund'])->nullable(false);
            $table->enum('transaction',['debit','credit'])->nullable(false);
            $table->decimal('summ', 14, 4)->nullable(false);
            $table->dateTime('created_at')->nullable(false);            
            $table->foreign('purse_id')->references('id')->on('purse');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation');
    }
}
