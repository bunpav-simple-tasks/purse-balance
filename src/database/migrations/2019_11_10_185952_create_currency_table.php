<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('valute_id',10)->nullable(false);
            $table->smallInteger('valute_num_code')->nullable(false);
            $table->string('valute_char_code',3)->nullable(false);
            $table->smallInteger('valute_nominal')->nullable(false);
            $table->string('valute_name',20)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency');
    }
}
