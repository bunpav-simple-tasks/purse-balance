<?php

namespace App\Services\PurseService\Mutator;

use App\Services\CurrencyLoader\CurrencyHistoryModel;
use App\Services\PurseService\Contract\ConvertInterface;

class UsdToRubConverter implements ConvertInterface
{
    /**
     * @var CurrencyHistoryModel
     */
    private $historyModel;

    public function __construct(CurrencyHistoryModel $currencyHistoryModel)
    {
        $this->historyModel = $currencyHistoryModel;
    }

    /**
     * @inheritDoc
     */
    public function convert(float $value) : float
    {
        $currentValue = current($this->historyModel->orderBy('id', 'desc')->limit(1)->get()->toArray())['value'];
        return $value * $currentValue;
    }
}
