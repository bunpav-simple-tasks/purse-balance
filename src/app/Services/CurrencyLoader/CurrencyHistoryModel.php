<?php
namespace App\Services\CurrencyLoader;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель таблицы валют
 **/
class CurrencyHistoryModel extends Model
{
    public $timestamps = false;

    protected $table = 'currency_history';

    protected $fillable = ['currency_id','value','created_at'];

    protected $attributes = [
        'created_at' => '',
        'currency_id' => null,
        'value' => null
    ];

    /**
     * @return int|null
     */
    public function getId():?int
    {
        return $this->getAttributeValue('id');
    }

    /**
     * @param int|null $value
     * @return $this
     */
    public function setId(?int $value)
    {
        $this->setAttribute('id', $value);
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyId():int
    {
        return $this->getAttributeValue('currency_id');
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCurrencyId(int $value)
    {
        $this->setAttribute('currency_id', $value);
        return $this;
    }

    /**
     * @return float
     */
    public function getValue():float
    {
        return $this->getAttributeValue('value');
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setValueId(float $value)
    {
        $this->setAttribute('value', $value);
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function getCreatedAt():\DateTimeImmutable
    {
        $created = $this->getAttributeValue('created_at');
        return new \DateTimeImmutable($created);
    }
}
