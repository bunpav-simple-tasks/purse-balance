<?php

namespace App\Services\PurseService;

use Illuminate\Database\Eloquent\Model;
use App\Services\CurrencyLoader\CurrencyModel;
use App\Services\CurrencyLoader\Dto\CurrencyDto;

/**
 * Модель таблицы кошельков
 **/
class PurseModel extends Model
{
    public $timestamps = false;

    protected $table = 'purse';

    protected $attributes = [
        'currency_id' => 0,
        'balance' => 0,
    ];


    /**
     * @return CurrencyDto
     */
    public function getCurrency(): CurrencyDto
    {
        $currency = CurrencyModel::find($this->getCurrencyId())->toArray();
        return (new CurrencyDto())->loadFromDb($currency);
    }

    /**
     * @return int|null
     */
    public function getId():?int
    {
        return $this->getAttributeValue('id');
    }

    /**
     * @param int|null $value
     * @return $this
     */
    public function setId(?int $value)
    {
        $this->setAttribute('id', $value);
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyId():int
    {
        return $this->getAttributeValue('currency_id');
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCurrencyId(int $value)
    {
        $this->setAttribute('currency_id', $value);
        return $this;
    }

    /**
     * @return float
     */
    public function getBalance():float
    {
        return $this->getAttributeValue('balance');
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setBalance(float $value)
    {
        $this->setAttribute('balance', $value);
        return $this;
    }
}
