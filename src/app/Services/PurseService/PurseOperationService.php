<?php

namespace App\Services\PurseService;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Class PurseOperationService
 * @package App\Services\PurseService
 */
class PurseOperationService implements Contract\OperationServiceInterface, Contract\BalanceInfoInterface
{
    private $pdo;
    private $purseModel;
    private $summator;

    public function __construct(
        Capsule $pdo,
        PurseModel $purseModel
    ) {
        $this->pdo = $pdo->getConnection();
        $this->purseModel = $purseModel;
    }

    /**
     * @param Contract\SummatorInterface $summator
     */
    public function setSummator(Contract\SummatorInterface $summator)
    {
        $this->summator = $summator;
    }

    /**
     * @inheritDoc
     */
    public function balance(int $purseId): Dto\PurseDto
    {
        $result = new Dto\PurseDto();
        /** @var PurseModel $purse */
        $purse = $this->purseModel->find($purseId);
        if ($purse === null) {
            throw new Exception\UndefinedPurseException('Не известный кошелек!');
        }
        $result ->setPurseId($purse->getId())
                ->setCurrencyCode($purse->getCurrency()->getCharCode())
                ->setBalance($purse->getBalance());
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function create(Dto\OperationDto $operationDto): Dto\PurseDto
    {
        if ($this->summator === null) {
            throw new Exception\EmptySummatorException('необходимо установить сумматор');
        }

        $result = new Dto\PurseDto();
        $result ->setPurseId($operationDto->getPurseId())
                ->setCurrencyCode($operationDto->getCurrencyCode());

        $this->pdo->beginTransaction();

        try {
            $purse = $this->purseModel->find($operationDto->getPurseId());

            $this->pdo->table('operation')->insert($operationDto->toDbArray());
            
            $result->setBalance($this->summator->summ($purse->getBalance(), $operationDto->getSumm()));

            if ($result->getBalance() < 0) {
                throw new Exception\BadBalanceException('Не достаточно средств для выполнения операции!');
            }

            $this->pdo->table('purse')->where('id', $result->getPurseId())->update(['balance' => $result->getBalance()]);

            $this->pdo->commit();
        } catch (\Throwable $e) {
            $this->pdo->rollback();
            throw $e;
        }

        return $result;
    }
}
