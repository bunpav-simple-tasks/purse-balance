<?php

namespace App\Services\PurseService\Dto;

use App\Services\Traits\JsonSeriliazableTrait;

/**
 * Class PurseDto
 * Структура кошелька
 *
 * @package App\Services\PurseService\Dto
 */
class PurseDto implements \JsonSerializable
{
    use JsonSeriliazableTrait;

    /** @var int */
    protected $purseId;
    /** @var float */
    protected $balance;
    /** @var string */
    protected $currencyCode;

    public function getCurrencyCode():string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $value):PurseDto
    {
        $this->currencyCode = $value;
        return $this;
    }


    public function getPurseId():int
    {
        return $this->purseId;
    }

    public function setPurseId(int $value):PurseDto
    {
        $this->purseId = $value;
        return $this;
    }

    public function getBalance():float
    {
        return $this->balance;
    }

    public function setBalance(float $value):PurseDto
    {
        $this->balance = $value;
        return $this;
    }
}
