# Purse Balance

Реализация методов API для работы с кошельком пользователя

Ограничения:
--

- У пользователя может быть только один кошелек.
- Поддерживаемые валюты: USD и RUB.
- При вызове метода для изменения кошелька на сумму с отличной валютой от валюты кошелька, сумма должна конвертироваться по курсу. 
- Курсы обновляются периодически.
- Все изменения кошелька должны фиксироваться в БД.

Задачи
--
 - Метод для изменения баланса 
   Обязательные параметры метода:
    * ID кошелька (например: 241, 242)
    * Тип транзакции (debit или credit)
    * Сумма, на которую нужно изменить баланс
    * Валюта суммы (допустимы значения: USD, RUB)
    * Причина изменения счета (например: stock, refund). Список причин фиксирован.
 - Метод для получения текущего баланса
   Обязательные параметры метода:
    *ID кошелька (например: 241, 242)
 - SQL запрос
    * [Написать SQL запрос](./src/sql.md), который вернет сумму, полученную по причине refund за последние 7 дней.


Технические требования
--
- Серверная логика должна быть написана на PHP версии >=7.0
- Для хранения данных должна использоваться реляционная СУБД
- Должны быть инструкции для развертывания проекта


Допущения
--

- Выбор дополнительных технологий не ограничен;
- Все спорные вопросы в задаче может и должен решать Исполнитель;

Развёртыване проекта
--
- git clone https://gitlab.com/bunpav-simple-tasks/purse-balance.git ./
- cp ./.env.example ./.env
- docker-compose up -d
- docker exec -it purse_balance_php-app_1 /bin/sh
- composer install
- php artisan migrate:refresh --seed
- php artisan import:currency

`purse_balance` - должно подхватиться из окружения, если это не так - посмотреть название контейнера и подставить его

Тесты
--
* [Установить IP контейнера с nginx](./src/tests/acceptance.suite.yml)
* Выполнить тесты с отправкой запросов на dev `./vendor/bin/codecept run --env dev`
* Выполнить тесты с отправкой запросов на stage `./vendor/bin/codecept run --env stage`


Curl Для проверки
--
Баланс
```
curl -H "Content-Type: application/json" -X GET "http://`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' purse_balance_nginx_1`/balance/242/"
```
Пополнение
```
curl -d '{"id":241,"transaction":"debit","summ":50,"currency":"RUB","cause":"refund"}' -H "Content-Type: application/json" -X POST "http://`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' purse_balance_nginx_1`/balance/"
```
