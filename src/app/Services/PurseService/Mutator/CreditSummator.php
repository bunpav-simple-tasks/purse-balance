<?php

namespace App\Services\PurseService\Mutator;

use App\Services\PurseService\Contract\SummatorInterface;

class CreditSummator implements SummatorInterface
{
    /**
     * @inheritDoc
     */
    public function summ(float $purseBalance, float $operationSumm): float
    {
        return $purseBalance - $operationSumm;
    }
}
