<?php

namespace App\Services\PurseService;

use App\Services\PurseService\Dto\PurseDto;
use App\Services\PurseService\Dto\OperationDto;
use App\Services\PurseService\Mutator\DebitSummator;
use App\Services\PurseService\Mutator\CreditSummator;
use App\Services\PurseService\Decorator\ConverterDecorator;
use App\Services\PurseService\Contract\BalanceInfoInterface;
use App\Services\PurseService\Contract\OperationServiceInterface;

/**
 * Class PurseOperationFasade
 * Более простой интерфейс для работы с кошельком
 *
 * @package App\Services\PurseService
 */
class PurseOperationFasade implements OperationServiceInterface, BalanceInfoInterface
{
    private $service;

    public function __construct(PurseOperationService $service)
    {
        $this->service = $service;
    }

    /**
     * @inheritDoc
     */
    public function create(OperationDto $operationDto):PurseDto
    {
        $this->service->setSummator(
            $operationDto->getTransaction() === 'debit'
            ? new DebitSummator()
            : new CreditSummator()
        );

        $decorator = app()->make(ConverterDecorator::class, ['service'=>$this->service]);
       
        return $decorator->create($operationDto);
    }

    /**
     * @inheritDoc
     */
    public function balance(int $purseId): PurseDto
    {
        return $this->service->balance($purseId);
    }
}
