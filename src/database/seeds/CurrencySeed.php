<?php

use Illuminate\Database\Seeder;
use App\Services\CurrencyLoader\CurrencyModel;

class CurrencySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createValute(CurrencyModel::USD_ID, CurrencyModel::USD_CODE, 840, 1, 'Доллар США');
        $this->createValute('4217', CurrencyModel::RUB_CODE, 643, 1, 'Российский рубль');
    }

    private function createValute(string $valuteId, string $valuteCode, int $valuteNumCode, int $nominal, string $name){
        $currency  = new CurrencyModel();
        $currency->setValuteId($valuteId)
                ->setValuteCharCode($valuteCode)
                ->setValuteNumCode($valuteNumCode)
                ->setValuteNominal($nominal)
                ->setValuteName($name);
        $currency->save();
    }
}
