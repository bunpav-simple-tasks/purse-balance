<?php
namespace Tests\MockTraits;

use App\Services\CurrencyLoader\CurrencyHistoryModel;

trait CurrencyHistoryMockTrait
{
    private function getCurrencyHistoryModelMock(): CurrencyHistoryModel {
        $model = \Mockery::mock(CurrencyHistoryModel::class);
        $model->allows([
            'orderBy' => $model,
            'limit'   => $model,
            'get'     => $model,
            'toArray' => [
                [ 'created_at' => date('Y-m-d'), 'currency_id' => 1, 'value' => 30 ]
            ]
        ]);

        return $model;
    }
}