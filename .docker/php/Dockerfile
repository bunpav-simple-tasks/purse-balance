FROM php:7.3-fpm-alpine

# Setup user
ENV HOME_DIR="/home/www-data"
RUN deluser www-data
RUN addgroup -g 1000 www-data &&  \
    adduser -u 1000 -g 1000 -G www-data -s /bin/bash -D -h ${HOME_DIR} www-data

RUN chown -R www-data:www-data /home/www-data && \
        chmod 700 /home/www-data

RUN set -xe \
    && apk add --update \
        icu \
        make \
        $PHPIZE_DEPS \
        zlib-dev \
        icu-dev \
        g++ \
        postgresql-dev \
        libzip-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install \
        intl \
        pdo_pgsql \
        zip \
    && rm -rf /tmp/* /usr/local/lib/php/doc/* /var/cache/apk/*


ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


RUN set -xe \
    && mkdir -p "/tmp" \
    # install composer
    && php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');" \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/bin --filename=composer \
    && composer --ansi --version --no-interaction \
    && composer --no-interaction global require 'hirak/prestissimo' \
    && composer clear-cache \
    && rm -rf /tmp/composer-setup.php /tmp/.htaccess \
    # show php info

USER www-data
WORKDIR /var/www/vhosts/purse/src

CMD ["php-fpm"]

EXPOSE 9000
