<?php


namespace Tests\Acceptance\Api;


use AcceptanceTester;
use Exception;

class BalanceCest
{
    public function checkStructureResponse(AcceptanceTester $I)
    {

        $I->setHeader('Accept','application/json');
        $I->amOnPage('/balance/241/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"balance"');

        $balance = \GuzzleHttp\json_decode($I->grabResponse(),true);


        if ( empty($balance['data'])
            || !array_key_exists('balance',$balance['data'])
            || !array_key_exists('currencyCode',$balance['data'])
            || !array_key_exists('purseId',$balance['data'])
        )

        {
            throw new Exception('Нарушена структура ответа!');
        }
    }

    public function checkValidPurseId(AcceptanceTester $I)
    {

        $I->setHeader('Accept','application/json');
        $I->amOnPage('/balance/241/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"balance"');

        $balance = \GuzzleHttp\json_decode($I->grabResponse(),true);

        if ( $balance['data']['purseId'] !== 241 )
        {
            throw new Exception('Вернули не то что просили!');
        }
    }

    public function checkNotExistPurseId(AcceptanceTester $I)
    {

        $I->setHeader('Accept','application/json');
        $I->amOnPage('/balance/-1/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"balance"');

        $balance = \GuzzleHttp\json_decode($I->grabResponse(),true);

        if ( empty($balance['message']) )
        {
            throw new Exception('Не получили сообщения о проблеме');
        }
    }




}