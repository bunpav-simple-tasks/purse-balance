<?php

namespace App\Services\PurseService\Dto;

/**
 * Class OperationDto
 * Структура операции
 *
 * @package App\Services\PurseService\Dto
 */
class OperationDto
{
    /** @var string */
    protected $transaction;
    /** @var string */
    protected $cause;
    /** @var float */
    protected $summ;
    /** \DateTimeInterface */
    protected $createdAt;
    /** @var int */
    protected $purseId;
    /** @var string */
    protected $currencyCode;

    /**
     * @return string
     */
    public function getCurrencyCode():string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $value
     * @return OperationDto
     */
    public function setCurrencyCode(string $value):OperationDto
    {
        $this->currencyCode = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransaction():string
    {
        return $this->transaction;
    }

    /**
     * @return string
     */
    public function getCause():string
    {
        return $this->cause;
    }

    /**
     * @return float
     */
    public function getSumm():float
    {
        return $this->summ;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt():\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getPurseId():int
    {
        return $this->purseId;
    }

    /**
     * @param string $value
     * @return OperationDto
     */
    public function setTransaction(string $value):OperationDto
    {
        $this->transaction = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return OperationDto
     */
    public function setCause(string $value):OperationDto
    {
        $this->cause = $value;
        return $this;
    }

    /**
     * @param float $value
     * @return OperationDto
     */
    public function setSumm(float $value):OperationDto
    {
        $this->summ = $value;
        return $this;
    }

    /**
     * @param \DateTimeInterface $value
     * @return OperationDto
     */
    public function setCreatedAt(\DateTimeInterface $value):OperationDto
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @param int $value
     * @return OperationDto
     */
    public function setPurseId(int $value):OperationDto
    {
        $this->purseId = $value;
        return $this;
    }


    /**
     * Возвращшает представление для DB
     * @return array
     */
    public function toDbArray()
    {
        $result = \get_object_vars($this);
        unset($result['purseId'],$result['createdAt'],$result['currencyCode']);
        $result['purse_id'] = $this->purseId;
        $result['created_at'] = $this->createdAt->format('Y-m-d H:i:s');

        return $result;
    }
}
