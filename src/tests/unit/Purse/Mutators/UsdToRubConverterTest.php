<?php
namespace Tests\Purse\Mutators;

use Tests\MockTraits\CurrencyHistoryMockTrait;
use App\Services\PurseService\Mutator\UsdToRubConverter;
use PHPUnit\Framework\TestCase;

class UsdToRubConverterTest extends TestCase
{
    use CurrencyHistoryMockTrait;

    /** @var UsdToRubConverter */
    private $converter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->converter = new UsdToRubConverter($this->getCurrencyHistoryModelMock());
    }

    /**
     * @dataProvider values
     * @param float $value
     */
    public function testConvert(float $value)
    {
        $currencyRate = 30;
        $this->assertEquals($this->converter->convert($value), $value * $currencyRate);
    }

    public function values(): array
    {
        return [
            [1],
            [50],
            [78],
            [12.2]
        ];
    }
}
