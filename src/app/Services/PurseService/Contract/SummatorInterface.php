<?php

namespace App\Services\PurseService\Contract;

/**
 * Interface SummatorInterface
 * Интерфейс для дебет/кредитовых операций
 *
 * @package App\Services\PurseService\Contract
 */
interface SummatorInterface
{
    /**
     * Вычисляет итоговый баланс
     *
     * @param float $purseBalance
     * @param float $operationSumm
     * @return float
     */
    public function summ(float $purseBalance, float $operationSumm): float;
}
