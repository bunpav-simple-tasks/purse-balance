<?php

namespace App\Services\CurrencyLoader\Dto;

/**
 * Class CurrencyDto
 * Структура данных валюты
 *
 * @package App\Services\CurrencyLoader\Dto
 */
class CurrencyDto
{
    /** @var int */
    protected $currencyId;
    /** @var string */
    protected $valuteId;
    /** @var int */
    protected $numCode;
    /** @var string */
    protected $charCode;
    /** @var int */
    protected $nominal;
    /** @var string */
    protected $name;
    /** @var float */
    protected $value;

    /**
     * @return float
     */
    public function getValue():float
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return CurrencyDto
     */
    public function setValue(float $value): CurrencyDto
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CurrencyDto
     */
    public function setName(string $name): CurrencyDto
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getNominal():int
    {
        return $this->nominal;
    }

    /**
     * @param int $nominal
     * @return CurrencyDto
     */
    public function setNominal(int $nominal): CurrencyDto
    {
        $this->nominal = $nominal;

        return $this;
    }

    /**
     * @return string
     */
    public function getCharCode(): string
    {
        return $this->charCode;
    }

    /**
     * @param string $charCode
     * @return CurrencyDto
     */
    public function setCharCode(string $charCode): CurrencyDto
    {
        $this->charCode = $charCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumCode(): int
    {
        return $this->numCode;
    }

    /**
     * @param int $numCode
     * @return CurrencyDto
     */
    public function setNumCode(int $numCode): CurrencyDto
    {
        $this->numCode = $numCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getValuteId(): string
    {
        return $this->valuteId;
    }

    /**
     * @param string $currencyId
     * @return CurrencyDto
     */
    public function setValuteId(string $currencyId): CurrencyDto
    {
        $this->valuteId = $currencyId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    /**
     * @param int $currencyId
     * @return CurrencyDto
     */
    public function setCurrencyId(int $currencyId): CurrencyDto
    {
        $this->currencyId = $currencyId;

        return $this;
    }

    /**
     * Загружает данные в структуру на основании данных из DB
     * @param $array
     * @return CurrencyDto
     */
    public function loadFromDb($array)
    {
        $result = new static;
        $result->setCurrencyId($array['id'])
        ->setValuteId($array['valute_id'])
        ->setNumCode($array['valute_num_code'])
        ->setCharCode($array['valute_char_code'])
        ->setName($array['valute_name'])
        ->setNominal($array['valute_nominal']);
        return $result;
    }
}
