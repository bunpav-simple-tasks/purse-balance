<?php
return [
    'fetch' => PDO::FETCH_CLASS,
    'default' => env('APP_DB_CONNECTION'),
    'connections' => [
        'pgsql' => [
            'driver'    => 'pgsql',
            'host'      => env('APP_DB_HOST'),
            'port'      => env('APP_DB_PORT'),
            'database'  => env('APP_DB_DATABASE', 'forge'),
            'username'  => env('APP_DB_USERNAME', 'forge'),
            'password'  => env('APP_DB_PASSWORD', ''),
            'charset'   => env('DB_CHARSET', 'utf8'),
            'collation' => env('DB_COLLATION', 'utf8_unicode_ci'),
            'prefix'    => env('DB_PREFIX', ''),
            'timezone'  => env('DB_TIMEZONE', '+00:00'),
        ],
        'default' => [
            'driver'    => 'pgsql',
            'host'      => env('APP_DB_HOST'),
            'port'      => env('APP_DB_PORT'),
            'database'  => env('APP_DB_DATABASE', 'forge'),
            'username'  => env('APP_DB_USERNAME', 'forge'),
            'password'  => env('APP_DB_PASSWORD', ''),
            'charset'   => env('DB_CHARSET', 'utf8'),
            'collation' => env('DB_COLLATION', 'utf8_unicode_ci'),
            'prefix'    => env('DB_PREFIX', ''),
            'timezone'  => env('DB_TIMEZONE', '+00:00'),
        ]
    ],
    'migrations' => 'migrations',
];