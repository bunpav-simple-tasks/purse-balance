<?php

namespace App\Services\PurseService\Contract;

use App\Services\PurseService\Dto\PurseDto;
use App\Services\PurseService\Dto\OperationDto;

/**
 * Interface OperationServiceInterface
 * @package App\Services\PurseService\Contract
 */
interface OperationServiceInterface
{
    /**
     * Создаёт платёжную операцию
     *
     * @param OperationDto $operationDto
     * @return PurseDto
     */
    public function create(OperationDto $operationDto): PurseDto;
}
