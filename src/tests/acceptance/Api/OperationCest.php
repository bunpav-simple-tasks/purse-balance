<?php


namespace Tests\Acceptance\Api;


use AcceptanceTester;
use Exception;

class OperationCest
{
    public function checkStructureResponse(AcceptanceTester $I) {
        $I->wantTo('create operation wis purse');
        $I->sendPOST('/balance/', [
            'id' => 241,
            'transaction' => 'debit',
            'summ' => 100,
            'currency' => 'RUB',
            'cause' => 'stock',
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"operation"');

        $balance = \GuzzleHttp\json_decode($I->grabResponse(),true);


        if ( empty($balance['data'])
            || !array_key_exists('balance',$balance['data'])
            || !array_key_exists('currencyCode',$balance['data'])
            || !array_key_exists('purseId',$balance['data'])
        )

        {
            throw new Exception('Нарушена структура ответа!');
        }
    }

    public function checkSuccessOperation(AcceptanceTester $I) {
        $I->wantTo('create operation wis purse RUB to RUB');
        $I->sendGET('/balance/242/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"balance"');

        $currentBalance = \GuzzleHttp\json_decode($I->grabResponse(),true)['data']['balance'];
        $appendSumm = 100;

        $I->sendPOST('/balance/', [
            'id' => 242,
            'transaction' => 'debit',
            'summ' => $appendSumm,
            'currency' => 'RUB',
            'cause' => 'stock',
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"operation"');

        $newBalance = \GuzzleHttp\json_decode($I->grabResponse(),true)['data']['balance'];


        if ( ($currentBalance +$appendSumm) !== $newBalance)

        {
            throw new Exception('Не произошло изменение баланса!');
        }
    }

    public function checkSuccessCurrencyOperation(AcceptanceTester $I) {
        $I->wantTo('create operation wis purse RUB to RUB');
        $I->sendGET('/balance/242/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"balance"');

        $currentBalance = \GuzzleHttp\json_decode($I->grabResponse(),true)['data']['balance'];
        $appendSumm = 10;

        $I->sendPOST('/balance/', [
            'id' => 242,
            'transaction' => 'debit',
            'summ' => $appendSumm,
            'currency' => 'USD',
            'cause' => 'stock',
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('"operation":"operation"');

        $newBalance = \GuzzleHttp\json_decode($I->grabResponse(),true)['data']['balance'];

        if ( ($currentBalance +$appendSumm) === $newBalance)

        {
            throw new Exception('Сумма зачисления должна была быть в валюте!');
        }
    }


}