<?php
namespace Tests\Purse;

use App\Services\PurseService\Exception\BadBalanceException;
use App\Services\PurseService\Exception\EmptySummatorException;
use App\Services\PurseService\Exception\UndefinedPurseException;
use App\Services\PurseService\PurseOperationService;
use App\Services\PurseService\Dto\PurseDto;
use App\Services\PurseService\Dto\OperationDto;

use PHPUnit\Framework\TestCase;
use Tests\MockTraits\ConnectionMockTrait;
use Tests\MockTraits\PurseModelMockTrait;

class PurseOperationServiceTest extends TestCase
{
    use ConnectionMockTrait;
    use PurseModelMockTrait;

    protected function setUp(): void
    {
        parent::setUp();
    }

    private function getTestOperationDto(): OperationDto {
        $dto = $this->getTestPurseDto();
        return (new OperationDto())->setPurseId($dto->getPurseId())
                                   ->setCurrencyCode($dto->getCurrencyCode())
                                ->setCause('stock')
            ->setSumm(100)
            ->setTransaction('debit')
            ->setCreatedAt(new \DateTimeImmutable());
    }


    /**
     * Проверяем что баланс приходит
     */
    public function testBalance()
    {
        $dto = $this->getTestPurseDto();
        $service = new PurseOperationService($this->getCapsuleMock(), $this->getPurseModelMock());

        $balance = $service->balance($dto->getPurseId());

        $this->assertInstanceOf(PurseDto::class,$balance);
        $this->assertEquals($balance->getPurseId(),$dto->getPurseId());
        $this->assertEquals($balance->getBalance(),$dto->getBalance());
        $this->assertEquals($balance->getCurrencyCode(),$dto->getCurrencyCode());

    }

    /**
     * Проверяем что отлавливаем моент с неизвестным кошельком
     */
    public function testFailBalance()
    {
        $purseModel = $this->getPurseModelMock();
        $purseModel->allows()->find(100)->andReturn(null);
        $service = new PurseOperationService($this->getCapsuleMock(), $purseModel);
        try {
            $service->balance(100);
            $this->assertTrue(true,'Не поймали ошибку не известного кошелька');
        } catch (UndefinedPurseException $error) {
            $this->assertTrue(true,'Поймали ошибку не известного кошелька');
        }
    }


    /**
     * Проверяем что без нужных зависимостей получим ожидаемые ошибки
     *
     * @throws \Throwable
     */
    public function testCreateEmptySummator() {
        $service = new PurseOperationService($this->getCapsuleMock(), $this->getPurseModelMock());
        try {
            $service->create($this->getTestOperationDto());
            $this->assertTrue(true,'Не поймали ошибку не неустановленного сумматора');
        } catch (EmptySummatorException $error) {
            $this->assertTrue(true,'Поймали ошибку не неустановленного сумматора');
        }
    }

    /**
     * Проверяем операцию пополнения
     * @throws \Throwable
     */
    public function testCreate()
    {
        $service = new PurseOperationService($this->getCapsuleMock(), $this->getPurseModelMock());
        $testPurseDto = $this->getTestPurseDto();
        $operationDto = $this->getTestOperationDto();
        $summ = $testPurseDto->getBalance() + $operationDto->getSumm();

        $service->setSummator(new \App\Services\PurseService\Mutator\DebitSummator());
        $balance = $service->create($operationDto);

        $this->assertInstanceOf(PurseDto::class,$balance);
        $this->assertEquals($balance->getPurseId(),$testPurseDto->getPurseId());
        $this->assertEquals($balance->getBalance(),$summ);
        $this->assertEquals($balance->getCurrencyCode(),$testPurseDto->getCurrencyCode());
    }

    /**
     * Проверяем что не удалось списать боле чем есть на балансе
     * @throws \Throwable
     */
    public function testCreateSmallBalance()
    {
        $service = new PurseOperationService($this->getCapsuleMock(), $this->getPurseModelMock());
        $operationDto = $this->getTestOperationDto();
        $operationDto->setTransaction('credit');
        $service->setSummator(new \App\Services\PurseService\Mutator\CreditSummator());
        try {
            $service->create($operationDto);
            $this->assertTrue(true,'Не поймали ошибку не достаточного баланса');
        } catch (BadBalanceException $error) {
            $this->assertTrue(true,'Поймали ошибку не достаточного баланса');
        }
    }
}
