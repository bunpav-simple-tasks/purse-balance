<?php

use Illuminate\Database\Seeder;
use App\Services\PurseService\PurseModel;

class PurseSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PurseModel())
              ->setBalance(0)
              ->setId(241)
              ->setCurrencyId(1)
              ->save();
        
        (new PurseModel())
              ->setBalance(0)
              ->setId(242)
              ->setCurrencyId(2)
              ->save();
        
    }
}
