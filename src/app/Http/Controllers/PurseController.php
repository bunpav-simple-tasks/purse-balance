<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PurseService\Dto\OperationDto;
use App\Services\PurseService\PurseOperationFasade;
use App\Services\PurseService\Exception\BadBalanceException;
use App\Services\PurseService\Exception\UndefinedPurseException;

class PurseController extends Controller
{
    /**
     * @var PurseOperationFasade
     */
    private $service;

    /**
     * PurseController constructor.
     * @param PurseOperationFasade $service
     */
    public function __construct(PurseOperationFasade $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $purseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function balance(int $purseId)
    {
        $result = $message = null;
        try {
            $result = $this->service->balance($purseId);
        } catch (UndefinedPurseException $error) {
            $message = $error->getMessage();
        } catch (\Throwable $error) {
            $code = 500;
            $message = 'Что-то пошло не так!';
        }

        return response()->json(['operation' => __FUNCTION__,'data'=>$result,'message'=>$message], $code ?? 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function operation(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'transaction' => 'required|string|in:debit,credit',
            'summ' => 'required|numeric',
            'currency' => 'required|string|in:USD,RUB',
            'cause' => 'required|string|in:stock,refund',
        ]);
        $result = $message = null;
        try {
            $operationDto = (new OperationDto())
            ->setCause($request->input('cause'))
            ->setPurseId((int) $request->input('id'))
            ->setTransaction($request->input('transaction'))
            ->setCurrencyCode($request->input('currency'))
            ->setSumm((float) $request->input('summ'))
            ->setCreatedAt(new \DateTimeImmutable(date('Y-m-d H:i:s')))
            ;
            $result = $this->service->create($operationDto);
        } catch (BadBalanceException $error) {
            $code = 500;
            $message = $error->getMessage();
        } catch (UndefinedPurseException $error) {
            $code = 500;
            $message = $error->getMessage();
        } catch (\Throwable $error) {
            $code = 500;
            $message = 'Что-то пошло не так!';
        }

        return response()->json(['operation' => __FUNCTION__,'data'=>$result,'message'=>$message], $code ?? 200);
    }
}
