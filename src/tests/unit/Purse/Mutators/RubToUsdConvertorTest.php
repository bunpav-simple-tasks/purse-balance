<?php
namespace Tests\Purse\Mutators;

use App\Services\PurseService\Mutator\RubToUsdConverter;
use PHPUnit\Framework\TestCase;
use Tests\MockTraits\CurrencyHistoryMockTrait;

class RubToUsdConvertorTest extends TestCase
{

    use CurrencyHistoryMockTrait;

    /** @var RubToUsdConverter */
    private $converter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->converter = new RubToUsdConverter($this->getCurrencyHistoryModelMock());
    }

    /**
     * @dataProvider values
     * @param float $value
     */
    public function testConvert(float $value)
    {
        $currencyRate = 30;
        $this->assertEquals($this->converter->convert($value), $value / $currencyRate);
    }

    public function values(): array
    {
        return [
            [1],
            [50],
            [78],
            [12.2]
        ];
    }
}
