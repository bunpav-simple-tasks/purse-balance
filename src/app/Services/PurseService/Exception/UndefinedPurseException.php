<?php

namespace App\Services\PurseService\Exception;

/**
 * Class UndefinedPurseException
 * Ошибка не известного кошелька
 *
 * @package App\Services\PurseService\Exception
 */
class UndefinedPurseException extends \LogicException
{
}
