<?php

namespace App\Services\CurrencyLoader;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель таблицы валют
 **/
class CurrencyModel extends Model
{
    public const USD_ID = 'R01235';
    public const USD_CODE = 'USD';
    public const RUB_CODE = 'RUB';

    public $timestamps = false;

    protected $table = 'currency';

    protected $attributes = [
        'valute_id' => '',
        'valute_num_code' => 0,
        'valute_char_code' => '',
        'valute_nominal' => '',
        'valute_name' => ''

    ];

    /**
     * @return int|null
     */
    public function getId():?int
    {
        return $this->getAttributeValue('id');
    }

    /**
     * @param int|null $value
     * @return $this
     */
    public function setId(?int $value)
    {
        $this->setAttribute('id', $value);
        return $this;
    }

    /**
     * @return string
     */
    public function getValuteId():string
    {
        return $this->getAttributeValue('valute_id');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValuteId(string $value)
    {
        $this->setAttribute('valute_id', $value);
        return $this;
    }

    /**
     * @return int
     */
    public function getValuteNumCode():int
    {
        return $this->getAttributeValue('valute_num_code');
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setValuteNumCode(int $value)
    {
        $this->setAttribute('valute_num_code', $value);
        return $this;
    }

    /**
     * @return string
     */
    public function getValuteCharCode():string
    {
        return $this->getAttributeValue('valute_char_code');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValuteCharCode(string $value)
    {
        $this->setAttribute('valute_char_code', $value);
        return $this;
    }

    /**
     * @return int
     */
    public function getValuteNominal():int
    {
        return $this->getAttributeValue('valute_nominal');
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setValuteNominal(int $value)
    {
        $this->setAttribute('valute_nominal', $value);
        return $this;
    }

    /**
     * @return string
     */
    public function getValuteName():string
    {
        return $this->getAttributeValue('valute_name');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValuteName(string $value)
    {
        $this->setAttribute('valute_name', $value);
        return $this;
    }

    /**
     * @param string $valuteId
     * @return Dto\CurrencyDto|null
     */
    public function findByValuteId(string $valuteId): ?Dto\CurrencyDto
    {
        $finded = $this->where('valute_id', $valuteId)->first();
        if (is_null($finded)) {
            return null;
        }
        $result = $finded->toArray();
        return (new Dto\CurrencyDto())->loadFromDb($result);
    }
}
