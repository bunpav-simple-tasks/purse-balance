<?php

namespace App\Services\Traits;

trait JsonSeriliazableTrait
{
    /**
     * Возвращает структуру объекта для json представления
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
