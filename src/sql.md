SQL вернёёт суммы в валютах полученной по причине `refund` за последние 7 дней 
```
SELECT c.valute_char_code, sum(o.summ)
FROM operation o
join purse p on p.id = o.purse_id
join currency c on c.id = p.currency_id
where cause = 'refund' and o.created_at > now() - interval '7 day'
group by c.valute_char_code;
```

SQL вернёёт сумму в рублях полученную по причине `refund` за последние 7 дней 
```
select 
sum(
	case when cur.valute_char_code = 'RUB' then cur.sum
	else cur.sum * (select value from currency_history order by id desc limit 1)
	end
)
from (
SELECT c.valute_char_code, sum(o.summ)
FROM operation o
join purse p on p.id = o.purse_id
join currency c on c.id = p.currency_id
where cause = 'refund' and o.created_at > now() - interval '7 day'
group by c.valute_char_code
) cur
```

TODO:
Можно подумать над более оптимальной выборкой для суммы по валютам

можно сделать "оптимизацию":
```
CREATE OR REPLACE FUNCTION public.get_sum_refund()
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
	declare 
		_kurs numeric(14,4);
	begin
		_kurs := (select value from currency_history order by id desc limit 1);
	
	return sum(
		case when o.purse_id = 241 then o.summ*_kurs
		else o.summ end
		) as summ
	FROM operation o
	where cause = 'refund' and o.created_at > now() - interval '7 day';

	end; 
 $function$
;

select * from get_sum_refund();
```
В данном контексте 241 - это ид кошелька с USD по этому умножаем его на текущий курс

