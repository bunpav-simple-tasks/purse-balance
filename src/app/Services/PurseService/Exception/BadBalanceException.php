<?php

namespace App\Services\PurseService\Exception;

/**
 * Class BadBalanceException
 * Ошибка не допустимого баланса
 *
 * @package App\Services\PurseService\Exception
 */
class BadBalanceException extends \LogicException
{
}
