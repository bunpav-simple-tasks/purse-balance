<?php
namespace App\Services\CurrencyLoader;

/**
 * Class CurrencyLoaderService
 * @package App\Services\CurrencyLoader
 */
class CurrencyLoaderService
{
    /**
     * @var CurrencyModel
     */
    private $currencyModel;
    /**
     * @var CurrencyHistoryModel
     */
    private $currecyHistoryModel;

    /**
     * CurrencyLoaderService constructor.
     * @param CurrencyModel $currencyModel
     * @param CurrencyHistoryModel $currecyHistoryModel
     */
    public function __construct(
        CurrencyModel $currencyModel,
        CurrencyHistoryModel $currecyHistoryModel
    ) {
        $this->currencyModel = $currencyModel;
        $this->currecyHistoryModel = $currecyHistoryModel;
    }

    /**
     * @param Dto\CurrencyDto $currencyDto
     * @return bool|void
     */
    public function dailyLoad(Dto\CurrencyDto $currencyDto)
    {
        $finded = $this->currencyModel->findByValuteId($currencyDto->getValuteId());
        if (is_null($finded) === true) {
            return;
        }

        $finded->setValue($currencyDto->getValue());

        return $this->loadHistory(date('Y-m-d'), $finded->getCurrencyId(), $finded->getValue());
    }

    /**
     * @param $date
     * @param $currencyId
     * @param $value
     * @return bool
     */
    private function loadHistory($date, $currencyId, $value)
    {
        $history = $this->currecyHistoryModel->where('currency_id', $currencyId)
            ->where('created_at', $date)
            ->first()
        ;

        if (is_null($history) !== true) {
            return false;
        }

        $this->currecyHistoryModel->newInstance([
            'currency_id'=> $currencyId,
            'value'=>$value,
            'created_at'=>$date
        ])->save();

        return true;
    }
}
