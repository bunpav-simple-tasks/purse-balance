<?php
namespace Tests\Purse\Mutators;

use App\Services\PurseService\Mutator\CreditSummator;
use PHPUnit\Framework\TestCase;

class CreditSummatorTest extends TestCase
{

    private $summator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->summator = new CreditSummator();
    }

    /**
     * @dataProvider dataProvider
     *
     * @param float $a
     * @param float $b
     */
    public function testSumm(float $a, float $b)
    {
        $summ = $this->summator->summ($a, $b);
        $this->assertEquals($summ, ($a - $b));
    }

    /**
     * @return array
     */
    public function dataProvider(): array
    {
        return [
            [1, 2],
            [5.5,10.85,]
        ];
    }
}
