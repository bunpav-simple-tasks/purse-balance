<?php
namespace Tests\Currency;

use App\Services\CurrencyLoader\CurrencyHistoryModel;
use App\Services\CurrencyLoader\CurrencyLoaderService;
use App\Services\CurrencyLoader\CurrencyModel;
use App\Services\CurrencyLoader\Dto\CurrencyDto;
use PHPUnit\Framework\TestCase;

class CurrencyLoaderServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    private function  getHistoryMock(bool $saved)
    {
        $dto = $this->getCurrencyDto();
        $currencyHistoryModel = \Mockery::mock(CurrencyHistoryModel::class);
        $today = date('Y-m-d');
        $currencyHistoryModel->allows()->where('currency_id', $dto->getCurrencyId())->andReturns($currencyHistoryModel);
        $currencyHistoryModel->allows()->where('created_at', $today)->andReturns($currencyHistoryModel);
        $currencyHistoryModel->allows()->first()->andReturns($saved ? null : $currencyHistoryModel);

        $currencyHistoryModel->allows()->save()->andReturns(true);

        $currencyHistoryModel->allows()->newInstance([
            'currency_id'=> $dto->getCurrencyId(),
            'value'=>$dto->getValue(),
            'created_at'=>$today
        ])->andReturns($currencyHistoryModel);

        return $currencyHistoryModel;
    }

    private function getCurrencyDto(): CurrencyDto
    {
        return (new CurrencyDto())
            ->setName('test')
            ->setCharCode('ttt')
            ->setCurrencyId(123)
            ->setNominal(1)
            ->setNumCode(123)
            ->setValue(11.5)
            ->setValuteId('test');
    }

    /**
     * Тестируем вариант когда информации нет в бд и метод сохраняет запись
     */
    public function testDailyLoad()
    {
        $currencyModel = \Mockery::mock(CurrencyModel::class);
        $currencyModel->allows()->findByValuteId('test')->andReturns($this->getCurrencyDto());
        $service = new CurrencyLoaderService($currencyModel, $this->getHistoryMock(true));
        $this->assertTrue($service->dailyLoad($this->getCurrencyDto()));
    }

    /**
     * Тестируем вариант когда информация есть в бд и метод не сохраняет запись
     */
    public function testDailyLoadNotSave()
    {
        $currencyModel = \Mockery::mock(CurrencyModel::class);
        $currencyModel->allows()->findByValuteId('test')->andReturns($this->getCurrencyDto());
        $service = new CurrencyLoaderService($currencyModel, $this->getHistoryMock(false));
        $this->assertFalse($service->dailyLoad($this->getCurrencyDto()));
    }
}
