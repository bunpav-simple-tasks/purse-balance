<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_history', function (Blueprint $table) {
            $table->increments('id');
            $table->date('created_at')->nullable(false);
            $table->decimal('value', 14, 4)->nullable(false);
            $table->smallInteger('currency_id')->nullable(false);
            $table->foreign('currency_id')->references('id')->on('currency');
            $table->unique(['created_at','currency_id'],'last_currency_update_uniq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_history');
    }
}
